package com.techl33t.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.techl33t.api.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User,Long >{

	

}
