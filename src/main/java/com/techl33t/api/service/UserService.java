package com.techl33t.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techl33t.api.entity.User;
import com.techl33t.api.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	public UserRepository userRepository;
	
	public List<User> getAllUsers() {
		List<User> user = new ArrayList<>();
		userRepository.findAll().forEach(data ->{
			user.add(data);
		});
		return user;
	}
	
	public User getUser(long UserId) {
		User user = new User();
		user = userRepository.findOne(UserId);
//		System.out.println(user.getToken());
//		Optional<User> opt = Optional.ofNullable(user);
		return user;
	}
	
	public String postUser(User user) {
		userRepository.save(user);
		return "ok";
	}
	
	public String putUser(long userId, User user) {
		userRepository.save(user);
		return "ok";
	}
}
