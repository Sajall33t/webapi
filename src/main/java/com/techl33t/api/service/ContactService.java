package com.techl33t.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techl33t.api.entity.Contact;
import com.techl33t.api.repository.ContactRepository;

@Service
public class ContactService {

	@Autowired
	public ContactRepository contactRepository;
	
	public List<Contact> getAllConatacts() {
		List<Contact> contact = new ArrayList<>();
		contactRepository.findAll().forEach(data ->{
			contact.add(data);
		});
		return contact;
	}
	
	public Contact getContact(long ContactId) {
		Contact contact = new Contact();
		contact = contactRepository.findOne(ContactId);
		return contact;
	}
	
	public String postContact(Contact contact) {
		contactRepository.save(contact);
		return "ok";
	}
	
	public String putContact(long ContactId, Contact contact) {
		contactRepository.save(contact);
		return "ok";
	}
}
