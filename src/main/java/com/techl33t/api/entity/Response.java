package com.techl33t.api.entity;

import java.util.Date;

public class Response<T> {

	private Date timestamp;
	private int status;
	private String error;
	private String exception;
	private String message;
	private T data;
	private String path;
	
	public Response(Date timestamp, int status, String error, String exception, String message, T data, String path) {
		super();
		this.timestamp = timestamp;
		this.status = status;
		this.error = error;
		this.exception = exception;
		this.message = message;
		this.data = data;
		this.path = path;
	}

	public Response(String message, String path) {
		super();
		this.status = 200;
		this.timestamp = new Date();
		this.error = "Not Found";
		this.exception = null;
		this.message = message;
		this.path = path;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String msg) {
		this.message = msg;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
