package com.techl33t.api.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class TokenMiddleware extends OncePerRequestFilter{

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String header = request.getHeader("Authorization");
//		if (header == null || !header.startsWith("Token ")) {
//            throw new RuntimeException("Token is missing");
//        }
//		String token = header.substring(6);
//		System.out.println("in middleware token=>"+token+" url =>"+request.getRequestURI().toString());
//		
//		if(token.matches("zaq12wsx") || request.getRequestURI().matches("/api/v1/users")) {
//			filterChain.doFilter(request, response);
//		}else {
//			throw new RuntimeException("Token mismatch");
//		}
		
		filterChain.doFilter(request, response);
	}

}
