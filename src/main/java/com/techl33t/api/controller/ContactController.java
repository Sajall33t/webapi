package com.techl33t.api.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.techl33t.api.entity.Contact;
import com.techl33t.api.entity.Response;
import com.techl33t.api.exception.UserNotFoundException;
import com.techl33t.api.service.ContactService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api/v1/")
public class ContactController {
	
	private static String UPLOADED_FOLDER = "/home/l33tlpt5/eclipse-workspace/uploads/";
	private static final String FILE_NAME = "/home/l33tlpt5/eclipse-workspace/uploads/test.xlsx";

	@Autowired
	public ContactService contactService;
	
	Logger logger = LogManager.getLogger(ContactController.class);
	
	@RequestMapping(value="/contact", method=RequestMethod.GET)
	public ResponseEntity<Response> getConatacts(HttpServletRequest context) {
		
		logger.info("in getConatacts");
		
		List<Contact> contact = new ArrayList<>();
		contact = contactService.getAllConatacts();
		
		Response<List<Contact>> res = new Response<List<Contact>>("result fetched", context.getRequestURI().toString());
		res.setData(contact);
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/contact/{contactId}", method=RequestMethod.GET)
	public ResponseEntity<Response> getConatact(@PathVariable long contactId, HttpServletRequest context) {
		Contact contact = new Contact();
		contact = contactService.getContact(contactId);
		
		if(contact == null)
			throw new UserNotFoundException("user does not exist!");
			
		Response<Contact> res = new Response<Contact>("result fetched",context.getRequestURI().toString());
		res.setData(contact);
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/contact", method=RequestMethod.POST)
	public ResponseEntity<Response> postUser(@RequestBody Contact contact, HttpServletRequest context) {
//		System.out.println(user);
		contactService.postContact(contact);
		
		Response<String> res = new Response<String>("Data saved",context.getRequestURI().toString());
		res.setData("body");
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}

	@RequestMapping(value="/contact/{contactId}", method=RequestMethod.PUT)
	public ResponseEntity<Response> putUser(@PathVariable long contactId, @RequestBody Contact contact, HttpServletRequest context) {
		Contact c = contactService.getContact(contactId);
		
		c.setFirstName(contact.getFirstName());
		c.setLastName(contact.getLastName());
		
		contactService.putContact(contactId, c);
		
		Response<String> res = new Response<String>("Data updated",context.getRequestURI().toString());
		res.setData("body");
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
	
	@PostMapping("/contact/upload")
	public ResponseEntity<Response> contactUpload(HttpServletRequest context, @RequestParam("file") MultipartFile file) {
		
		Response<String> res = new Response<String>("file operation",context.getRequestURI().toString());
		String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
		System.out.println(extension);
		
		if(!extension.matches("xlsx")) {
			res.setData("file should be xlsx");
		}else if(file.isEmpty()) {
			res.setData("file is empty");
		}else {
			try {
				byte[] bytes = file.getBytes();
				Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
	            Files.write(path, bytes);
	            
			}
			catch(IOException e) {
				e.printStackTrace();
			}
			
			res.setData("file uploaded!");
		}
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
	
	@GetMapping("/contact/excel")
	public ResponseEntity<Response> excelReader(HttpServletRequest context) {
		Response<String> res = new Response<String>("excel read operation",context.getRequestURI().toString());
		
		try {
			FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            
            while (iterator.hasNext()) {
            	XSSFRow row = (XSSFRow) iterator.next();
            	Contact cont = new Contact();
            	
            	String add = row.getCell(0) != null ? row.getCell(0).toString() :"";
            	String fname = row.getCell(1) != null ? row.getCell(1).toString() :"";
            	String lname = row.getCell(2) != null ? row.getCell(2).toString() :"";
            	String midname = row.getCell(3) != null ? row.getCell(3).toString() :"";
            	String mob = row.getCell(4) != null ? row.getCell(4).getRawValue() :"";
            	String title = row.getCell(5) != null ? row.getCell(5).toString() :"";
            	
//                System.out.println(row.getCell(0)+"|"+row.getCell(1)+"|"+row.getCell(2)+"|"+row.getCell(3)+"|"+row.getCell(4).getRawValue()+"|"+row.getCell(5));
                cont.setAddress(add);
                cont.setFirstName(fname);
                cont.setLastName(lname);
                cont.setMiddleName(midname);
                cont.setMobile(mob);
                cont.setTitle(title);
                
                contactService.postContact(cont);
            }
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		res.setData("excel stored to db!");
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
}
