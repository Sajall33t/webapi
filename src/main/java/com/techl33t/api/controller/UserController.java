package com.techl33t.api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techl33t.api.entity.Response;
import com.techl33t.api.entity.User;
import com.techl33t.api.exception.UserNotFoundException;
import com.techl33t.api.service.UserService;


@RestController
@RequestMapping("/api/v1/")
public class UserController {

	@Autowired
	public UserService userService;
	
	Logger logger = LogManager.getLogger(UserController.class);
	
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public ResponseEntity<Response> getUsers(HttpServletRequest context) {
		
		logger.info("in getUsers");
		
		List<User> users = new ArrayList<>();
		users = userService.getAllUsers();
		
		Response<List<User>> res = new Response<List<User>>("result fetched", context.getRequestURL().toString());
		res.setData(users);
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/users/{userId}", method=RequestMethod.GET)
	public ResponseEntity<Response> getUser(@PathVariable long userId, HttpServletRequest context) {
		User user = new User();
		user = userService.getUser(userId);
		
		if(user == null)
			throw new UserNotFoundException("user does not exist!");
			
		Response<User> res = new Response<User>("result fetched",context.getRequestURL().toString());
		res.setData(user);
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/users", method=RequestMethod.POST)
	public ResponseEntity<Response> postUser(@RequestBody User user, HttpServletRequest context) {
		System.out.println(user);
		userService.postUser(user);
		
		Response<String> res = new Response<String>("Data saved",context.getRequestURL().toString());
		res.setData("body");
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}

	@RequestMapping(value="/users/{userId}", method=RequestMethod.PUT)
	public ResponseEntity<Response> putUser(@PathVariable long userId, @RequestBody User user, HttpServletRequest context) {
		User u = userService.getUser(userId);
		
		u.setFirstName(user.getFirstName());
		u.setLastName(user.getLastName());
		u.setMobileNo(user.getMobileNo());
		
		userService.putUser(userId, u);
		
		Response<String> res = new Response<String>("Data updated",context.getRequestURL().toString());
		res.setData("body");
		return new ResponseEntity<Response>(res, HttpStatus.OK);
	}
}
