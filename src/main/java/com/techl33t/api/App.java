package com.techl33t.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication (scanBasePackages= {"com.techl33t.api"})
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
